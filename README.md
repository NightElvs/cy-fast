
++++++++++++self说明++++++++++++

**2023/01/27**

1.cy-fast/src是最原始的项目文件fork别人的项目而来的;
2.cy-fastModified.zip为自己初版更改的工程仍然是maven的；
3.GRCFileSysBakJar.zip为自己初版更改的工程非maven版本;
4.GrcFileSys.zip为eclipse工程包(非maven版本)本地在eclipse中可以运行(符合需求的版本);
  说明:本地项目依赖的jar包需要tomcat的jar包，导出war的包中要可以剔除tomcat的jar包依赖，因为本身就是部署到了web容器tomcat中了
       如果后续需要部署到tomcat则也要剔除对应的tomcat的jar包依赖；
5.GrcFileSys.war为Tomcat可以部署的版本(部署包非工程包)
  注意：(1)WEB-INF下要有lib目录,且目录中需要有依赖的jar包，eclipse导出jar包后可以后续复制进war包,
       同时，META-INF中自动生成的MANIFEST.MF文件不用管(这个不同于导出jar包的形式，导出jar包需要
       手动修改)
       (2)修改tomcat配置
          context.xml:
          设置启动tomcat时静态资源缓存的最大值
          <Resources cachingAllowed="true" cacheMaxSize="100000"/>
          server.xml:
          设置localhost:8080直接访问项目，无需加项目名
          <Context path="" docBase="GrcFileSys" reloadable="true" privileged="true" />
       (3)war部署启动主类CyFastApplication.java一定要继承一下SpringBootServletInitializer，并重写configure方法；  
  说明:之所以不导出jar包,是因为jar形式运行的url路径不一样是含有!的形式,很多java代码需要，所以这里不这样处理了;        
6.apache-tomcat-8.0.53Test.zip的webapps下已经部署了GrcFileSys可以直接使用bin目录下的startup.bat启动
7.tomcat启动后访问地址为http://localhost:8080/login.html或http://localhost:8080
8.日志含有tomcat日志+logback.xml定义的日志路径

**2023/02/19**

1.GrcFileSysRel.zip为可用于部署到webloigc的工程包，后续可导出war包并解压成含
  META-INF 和 WEB-INF的目录，然后放到linux服务器对应的位置，用weblogic部署即可;
  注意手动把lib目录下依赖的jar包放入WEB-INF中;
  此工程包导出的war包进行基础性修改(如:加入依赖包，修改路径，日志清理时间等等)后可用于生产部署;
  
2.(在weblogic能上传文件这个问题很关键!!!)Springboot的文件上传接口，使用postman测试一直报Required request part 'file' is not present错误(真坑，自己是用这个解决的！！！)

  	(1)pom.xml内添加新的依赖
  	<dependency>
  	   <groupId>commons-fileupload</groupId>
  	   <artifactId>commons-fileupload</artifactId>
  	   <version>1.4</version>
  	</dependency>
  
  	(2)主要是#排除默认配置(这个可以不配置，和网友说的一样，主要是WebMvcConfig.java)
      spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration
  
    (3)public class WebMvcConfig implements WebMvcConfigurer {
  
      private static final Log log = LogFactory.getLog(WebMvcConfig.class);
  
      @Bean(name = "multipartResolver")
      public MultipartResolver multipartResolver() {
          log.info("Loading the multipart resolver");
              CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
          return multipartResolver;
      }
      @Override
      public void configurePathMatch(PathMatchConfigurer configurer) {
  
      }
  
     @Override
     public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
  
     }
  }
  详见(https://blog.csdn.net/u013231332/article/details/105624361)  

3.pomSelf.xml文件中升级log4j系列 和 logback系列 ,fastjson的版本 解决版本漏洞，同时加入
  commons-configuration,spring-boot-legacy的依赖;
  
4.工程手动引入了guava-27.0.1-jre.jar,json-lib-2.4-jdk15.jar

**2023/03/04**

1.GrcFileSysExtend.zip为拓展了查询服务器指定日期目录下文件列表功能的工程开发压缩包;

**开发过程中遇到的问题:**

        281.idea 本地调试，修改代码，代码自动生效
        详见(https://www.cnblogs.com/zqdf/p/15351978.html)
        
        282.idea修改代码没有立即生效怎么解决
        详见(https://blog.csdn.net/weixin_36067256/article/details/124041429)
        
        283.JAVA 读写JSON文件
        详见(https://blog.csdn.net/tianxingjian0509/article/details/114778232)
        
        284.使用idea断点调试时出现no executable code found at line问题
        详见(https://www.cnblogs.com/roobtyan/p/9576703.html)
        
        285.getOriginalFilename方法与“文件名、目录名或卷标语法不正确”
        	// 获取上传的文件名称，并结合存放路径，构建新的文件名称
        	String filename = file.getOriginalFilename();
        	
        	// 文件上传时，Chrome和IE/Edge对于originalFilename处理不同
        	// Chrome 会获取到该文件的直接文件名称，IE/Edge会获取到文件上传时完整路径/文件名
        	
        	// Check for Unix-style path
        	int unixSep = filename.lastIndexOf('/');
        	// Check for Windows-style path
        	int winSep = filename.lastIndexOf('\\');
        	// Cut off at latest possible point
        	int pos = (winSep > unixSep ? winSep : unixSep);
        	if (pos != -1)  {
        		// Any sort of path separator found...
        		filename = filename.substring(pos + 1);
        	}
        	
        	File filepath = new File(destination, filename);
        详见(https://blog.csdn.net/magi1201/article/details/86736648)
        
        286.$.ajax 中的contentType
            $.ajax contentType 和 dataType , contentType 主要设置你发送给服务器的格式，dataType设置你收到服务器数据的格式。
        
        	在http 请求中，get 和 post 是最常用的。在 jquery 的 ajax 中， contentType都是默认的值：application/x-www-form-urlencoded，
        	这种格式的特点就是，name/value 成为一组，每组之间用 & 联接，而 name与value 则是使用 = 连接。如： wwwh.baidu.com/q?key=fdsa&lang=zh
        	这是get , 而 post 请求则是使用请求体，参数不在 url 中，在请求体中的参数表现形式也是: key=fdsa&lang=zh的形式。
        
        	http 还可以自定义数据类型，于是就定义一种叫 application/json 的类型。这种类型是 text ， 我们 ajax 的复杂JSON数据，用 JSON.stringify序列化后，然后发送，在服务器端接到然后用 JSON.parse 进行还原就行了，这样就能处理复杂的对象了。
        详见(https://www.cnblogs.com/htoooth/p/7242217.html)	
        
        287.计算时间毫秒数(30*24*60*60*1000）得到负数
        	 如果写成30*24*60*60*1000得到的将是负数，因为int类已经溢出了，必须要用long类型
        	 //计算时间毫秒数，正确写法
             Long Interval=(long) (30L*24L*60L*60L*1000L);
        详见(https://www.ngui.cc/el/888321.html?action=onClick)
        
        288.SpringBoot 上传文件/图片返回下载路径(重要)
        详见(https://www.codenong.com/cs106225460/)
        
        289.@Controller和@RestController的区别？
        	知识点：@RestController注解相当于@ResponseBody ＋ @Controller合在一起的作用。
        
        	1) 如果只是使用@RestController注解Controller，则Controller中的方法无法返回jsp页面，或者html，配置的视图解析器 InternalResourceViewResolver不起作用，返回的内容就是Return 里的内容。
        
            2) 如果需要返回到指定页面，则需要用 @Controller配合视图解析器InternalResourceViewResolver才行。
            如果需要返回JSON，XML或自定义mediaType内容到页面，则需要在对应的方法上加上@ResponseBody注解。 
        详见(https://www.cnblogs.com/shuaifing/p/8119664.html)       
        
        
        290.转换json出现错误Cannot resolve net.sf.json-lib:json-lib:2.4
            maven上加上 <classifier>jdk15</classifier>（直接复制下面代码到pom.xml即可）
        详见(https://blog.csdn.net/weixin_43910668/article/details/115008509)
        
        291.IDEA如何导入jar包(基础+重要!!!)
        详见(https://blog.csdn.net/rej177/article/details/126892429)
        
        292./*和/**的区别
            /* 是拦截所有的文件夹，不包含子文件夹
        	/** 是拦截所有的文件夹及里面的子文件夹
        	如请求/usr/add/a，/*不会进行拦截，/**会拦截
        	而请求/usr/add，/*会进行拦截，/**也会拦截，
        
        	相当于/*只有后面一级
        
        	/** 可以包含多级
        详见(https://blog.csdn.net/qq_44761854/article/details/121579711)
        
        293.springboot项目的打包发布部署，jar和war的区别
        详见(https://blog.csdn.net/Rockandrollman/article/details/128070781)
        
        284.上下文路径request.getContextPath();与${pageContext.request.contextPath} (页面很多资源如png,json,controller方法访问不到的原因所在)
        详见(https://www.bbsmax.com/A/ke5jGyBgdr/)
            说明:如果使用html,可以使用js获取getContextPath()，理由：很多资源在开源包中url路径要一个个改
        
        285.jsp中的js中获取项目路径的方法(详细)
        详见(https://www.shuzhiduo.com/A/xl561mj1Jr/)
        
        286.JS文件中获取contextPath的方法(有用)
        详见(https://www.shuzhiduo.com/A/kPzOQ74Z5x/)
        
        287.[Maven基础]--使用Maven导出项目依赖的jar包(新+重要+基础！！！)
        详见(https://blog.51cto.com/u_13966077/5819526)
        
        288.org.springframework.beans.factory.BeanCreationException: Error creating bean with name ‘xxx‘
            缺少aspect，AOP的maven坐标
               <dependency>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-aop</artifactId>
                    <version>4.3.12.RELEASE</version>
                </dependency>
        
                <dependency>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-aspects</artifactId>
                    <version>4.3.12.RELEASE</version>
                </dependency>
        详见(https://blog.csdn.net/weixin_46822367/article/details/121715896)
        
        289.eclipse将项目打包成jar运行(基础+重要)
            使用eclipse将项目打包后，项目中实际调取的依赖和文件等路径一般需要和项目的jar存放到同一级目录，否则会出现ClassNotFound等异常，放在
            同一级目录也方便后面根据需求修改配置文件。
        
            注意：linux中需要用绝对路径（自定义lib包，执行pwd）否则报第一个错误。（在win10正常编译也可以执行但是在linux中用相对路径就是不行。）
        详见(https://blog.csdn.net/qq_36173194/article/details/82842531)
        详见(https://blog.csdn.net/xysxlgq/article/details/122862948)
        
        290.java获取jar包中的文件资源(重要+基础！！！)
             (jar中资源有其专门的URL形式： jar:!/{entry} )。所以，如果jar包中的类源代码用File f=new File(相对路径);的形式，是不可能定位到文件资源的。这也是为什么源代码打包成jar文件后，调用jar包时会报出
             FileNotFoundException的症结所在了。
          	 我们不能用常规操作文件的方法来读取ResourceJar.jar中的资源文件res.txt，但可以通过Class类的
             getResourceAsStream()方法来获取 ，这种方法是如何读取jar中的资源文件的，这一点对于我们来说是透明的
        详见(https://blog.csdn.net/weixin_44462773/article/details/124714272)
        
        291.Tomcat 管理员，用户名，密码配置
            修改\conf\tomcat-users.xml文件
            <?xml version='1.0' encoding='utf-8'?>
        	<tomcat-users>
        	    <role rolename="manager-gui"/>  
        	    <role rolename="manager-script"/>  
        	    <role rolename="manager-jmx"/>  
        	    <role rolename="manager-status"/>  
        	    <user username="admin" password="admin" roles="manager-gui,manager-script,manager-jmx,manager-status"/>
        	</tomcat-users>
        详见(https://blog.51cto.com/u_11103019/3767918)
        
        292.Eclipse导出War时,没有lib的jar包依赖
            导出war包时可以导出依赖的jar包
        详见(https://blog.csdn.net/gaoqingliang521/article/details/110475109)
        
        293.No cache or cacheManager properties have been set. Authorization cache cannot be obtained.
            ShiroConfig.java
            //配置自定义的权限登录器
            @Bean(name="authRealm")
            public AuthRealm authRealm(@Qualifier("credentialsMatcher") CredentialsMatcher matcher) {
                AuthRealm authRealm=new AuthRealm();
                authRealm.setCredentialsMatcher(matcher);
                
                //关闭授权缓存
                authRealm.setAuthenticationCachingEnabled(false);
                return authRealm;
            } 
        详见(https://blog.csdn.net/qq_40155654/article/details/88796688)
        
        294.springboot打war包部署到tomcat后无法访问的解决方法
            如果你要将项目打包成war包放在tomcat上执行，这个类得继承一下SpringBootServletInitializer，并重写configure方法，如下：
            @SpringBootApplication()
        	public class LocalserviceApplication extends SpringBootServletInitializer {
        
        	    public static void main(String[] args) {
        	        SpringApplication.run(LocalserviceApplication.class, args);
        	    }
        
        	    // 按照下面的方式重写
        	    @Override
        	    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        	        return builder.sources(LocalserviceApplication.class);
        	    }
        
        	}
        详见(https://blog.csdn.net/m0_67401417/article/details/124038750)
        
        295.Java class.getClassLoader().getResource("") 获取资源路径(基础！！！)
            1、xxx.class.getClassLoader().getResource(“”).getPath(); 
        	获取src资源文件编译后的路径（即classes路径） 
        
        	2、xxx.class.getClassLoader().getResource(“文件”).getPath(); 
        	获取classes路径下“文件”的路径 
        
        
        	3、xxx.class.getResource(“”).getPath()； 
        	缺少类加载器，获取xxx类经编译后的xxx.class路径 
        
        	4、this.getClass().getClassLoader().getResource(“”).getPath()； 
        	以上三种方法的另外一种写法 
        
        	5、request().getSession().getServletContext().getRealPath(“”)； 
        	获取web项目的路径
        详见(https://www.cnblogs.com/ncy1/articles/8811238.html)
        
        296.教你如何修改tomcat端口
            Tomcat默认端口是8080，访问项目时每次都要打上端口号，这样也太麻烦了，想偷点懒，怎么办？如果你想访问的时候不打上端口号，
            就可以进行tomcat配置文件中修改它的一个端口，tomcat的端口信息可在server.xml中修改
        详见(https://blog.csdn.net/qq_38571398/article/details/79964528)
          




 **框架说明** 

1. 基于springboot+shiro+freemarker的快速开发框架,代码结构清晰，快速上手使用！
2. 配置代码生成器，减少70%开发时间，专注业务逻辑。
3. 前端声明式组件封装、附带文档编写 ctrl+c ctrl+v 即可使用。封装数据源，可通过url、枚举、字典直接渲染组件。代码量极少且易维护。
4. layui常用代码的二次封装，省略layui部分繁琐的代码！
    
 例：

![输入图片说明](https://gitee.com/uploads/images/2017/1213/182658_b69a61e5_1334796.png "屏幕截图.png")


![输入图片说明](https://gitee.com/uploads/images/2017/1213/182830_c824ecdd_1334796.png "屏幕截图.png")


 **java** 


- common     
        - config         配置类 <br>
        - enumresource   枚举类<br>
        - exception      统一异常处理<br>
        - log            统一日志处理<br>
        - shiro          shiro相关<br>
        - utils          工具类<br>
        - xss            xss相关类<br>
       
- controller  

- dao   
   
- entity   
  
- service


 **resources** 

- db.migration     flyway sql脚本

- generator        代码生成器相关配置

- mapper           mapper文件

- static           静态文件

- templates        freemarker页面



 **如何启动**
 
1. 通过git下载源码
2. 创建数据库cy-fast，数据库编码为UTF-8（已配置flyway数据库版本管理，无需创建表）
3. IDEA、Eclipse导入项目
4. 启动 CyFastApplication 类
5. 项目访问路径：http://localhost:8080/
6. 账号密码：admin/admin



 **项目演示** 

- 演示地址：http://fast.cymall.xin
- 账号密码：admin/admin


- 官网首页 http://www.cymall.xin
- 项目主页 http://fast.cymall.xin
- 码云地址 https://gitee.com/leiyuxi/cy-fast
- 中国开源 https://www.oschina.net/p/cy-fast
- qq群    275846351


 **项目截图** 

后台管理页面

![输入图片说明](https://gitee.com/uploads/images/2018/0106/183812_8ac728a1_1334796.png "屏幕截图.png")



前端框架页面

![输入图片说明](https://gitee.com/uploads/images/2017/1226/173206_cf4ab878_1334796.png "屏幕截图.png")


 **组件使用说明页面** 

代码示例

![输入图片说明](https://gitee.com/uploads/images/2017/1213/155929_6041fc05_1334796.png "屏幕截图.png")

效果

![输入图片说明](https://gitee.com/uploads/images/2017/1213/155939_d335ad4a_1334796.png "屏幕截图.png")

参数详解

![输入图片说明](https://gitee.com/uploads/images/2017/1213/155950_a6543f8b_1334796.png "屏幕截图.png")

图片上传组件

![上传组件封装](https://gitee.com/uploads/images/2017/1213/181231_21621036_1334796.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1213/181319_13d218bc_1334796.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1213/181422_1ab20aee_1334796.png "屏幕截图.png")


 **捐赠作者** 

如有帮助到您，请作者喝杯咖啡吧！

![输入图片说明](https://gitee.com/uploads/images/2018/0106/184140_fd082023_1334796.png "屏幕截图.png")
